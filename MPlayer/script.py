'''MPlayer Node'''

### Libraries required by this Node
import threading
from time import sleep



### Parameters used by this Node

# -quiet      : reduces verbosity of console messages
# -msgmodule  : prepends module name to console messages

# status line for debug purposes
# A: 2.1 V: 2.2 A-V: -0.167 ct: 0.042 57/57 41% 0% 2.6% 0 4 49% 1.00x
# A: 2.1      (audio position in seconds)
# V: 2.2      (video position in seconds)
# A-V: -0.167 (audio-video difference in seconds (delay))
# ct: 0.042   (total A-V sync correction done)
# 57/57       (frames played/decoded (counting from last seek))
# 41%         (video codec CPU usage in percent)
# 0%          (video_out CPU usage)
# 2.6%        (audio codec CPU usage in percent)
# 0           (frames dropped to maintain A-V sync)
# 4           (current level of image postprocessing (when using -autoq))
# 49%         (current cache size used (around 50% is normal))
# 1.00x       (playback speed as a factor of original speed)

param_command = Parameter({ 'title': 'Additional Arguments',
						    'desc': 'The list of command line arguments as JSON',
						    'schema': { 'type': 'array', 'items': {
								'type': 'object',
							  	'properties': { 'arg': {'type': 'string'} }	} },
                'order': next_seq()})

param_playlist = Parameter({ 'title': 'Playlist',
						    'desc': 'The list of command line arguments as JSON',
						    'schema': { 'type': 'array', 'items': {
								'type': 'object',
							  	'properties': { 'arg': {'type': 'string'} }	} },
                'order': next_seq()})

param_debug = Parameter({ 'title': 'Debug Mode', 'desc': 'Increase feedback on playback.',
                          'schema': { 'type': 'boolean' }, 'order': next_seq()}) 

app = ['C:\Mplayer\mplayer.exe', '-slave', '-idle']
quiet = ['-quiet']

### Functions used by this node

# useful slave commands
# mute [value]          : (1 == on, 0 == off)
# pause                 : pauses playback of the current file

# debug orientated slave commands
# get_time_length       : print out length of current file in seconds
# get_time_pos          : print out the current position in the file in seconds
# get_audio_codec       : print out the name of the current file
# get_audio_bitrate     : print out the current bitrate of the file
# get_video_bitrate     : print out the video bitrate of the current file
# get_video_codec       : print out the video codec name of the current file
# get_video_resolution  : print out the video resolution of the current file

def command(arg):
  process.send(arg)
  
# Local actions this Node provides
def local_action_Command(arg = None):
  """{"title":"Command","schema":{"title":"Enter command...","type":"string","required":"true"},"group":"Custom"}"""
  print 'Action Command requested'
  command(arg)

def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"Mute playback of all files.","group":"Audio"}"""
  command('mute 1')

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"Unmute playback of all files.","group":"Audio"}"""
  command('mute 0')

def local_action_Pause(arg = None):
  """{"title":"Pause","desc":"Pause playback of current file.","group":"Playback"}"""
  command('pause')

def local_action_Stop(arg = None):
  """{"title":"Stop","desc":"Stop playback of all files.","group":"Playback"}"""
  command('stop')

  

### Process managed by this Node
process = Process([],
      started=lambda: console.info('Started!'),
      stdout=lambda arg: console.info('Got stdout line:' + arg),
      stdin=lambda arg: console.info(arg + ' was sent to process'),
      stderr=lambda arg: console.info('Got stderr line:' + arg),
      stopped=lambda arg: console.info('Stopped! exitCode:%s' % arg),
      timeout=lambda: console.warn('Request timeout!')) 



def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'
  
  #  Collect and set command parameters.
  commands = app
  
  if param_command:
    reducedCommand = [x['arg'] for x in param_command]
    print 'Using command %s' % reducedCommand
    # Combine original parameter (application location) with additional arguments
    commands = app + reducedCommand

  if param_debug is not True:
    commands = commands + quiet
   
  if param_playlist:
    playlist = [x['arg'] for x in param_playlist]
    for item in playlist:
      console.info(item)
    commands = commands + playlist
  
  process.setCommand(commands)
