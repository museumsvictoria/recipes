'''Raspberry Pi Node'''

### Libraries required by this Node
import subprocess



### Functions used by this Node
def shutdown():
  returncode = subprocess.call("sudo shutdown -h now", shell=True)
  
def restart():
  returncode = subprocess.call("sudo shutdown -r now", shell=True)

def mute():
  returncode = subprocess.call("sudo amixer cset numid=2 0", shell=True)

def unmute():
  returncode = subprocess.call("sudo amixer cset numid=2 1", shell=True)

# def set_volume(vol):
#   returncode = subprocess.call("sudo amixer cset numid=1 "+str(vol)+"%", shell=True)



### Local actions this Node provides
def local_action_PowerOff(arg = None):
  """{"title":"PowerOff","desc":"PowerOff","group":"Power"}"""
  print 'Action PowerOff requested.'
  shutdown()
  
def local_action_Restart(arg = None):
  """{"title":"Restart","desc":"Restart","group":"Power"}"""
  print 'Action Restart requested.'
  restart()

def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn","group":"Volume"}"""
  print 'Action MuteOn requested.'
  mute()

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff","group":"Volume"}"""
  print 'Action MuteOff requested.'
  unmute()
  
# def local_action_SetVolume(arg = None):
#   """{"title":"SetVolume","desc":"SetVolume","schema":{"title":"Level","type":"integer","required":"true"},"group":"Volume"}"""
#   print 'Action SetVolume requested - '+str(arg)
#   set_volume(arg)



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'
