# Title:    Two-way UDP Node
# Version:  1.2
# Date:     2016.06.16
# Author:   Julien de-Sainte-Croix

'''Max UDP Node'''

### Libraries required by this Node
import socket



### Local events this Node provides
local_event_EndGramophone = LocalEvent('{"title":"EndGramophone","desc":"EndGramophone","group":"Playback"}')
local_event_EndRadio = LocalEvent('{"title":"EndRadio","desc":"EndRadio","group":"Playback"}')
local_event_EndSymphonium = LocalEvent('{"title":"EndSymphonium","desc":"EndSymphonium","group":"Playback"}')



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address","desc":"The IP address.","schema":{"type":"string"}}')
param_port = Parameter('{"title":"Port","schema":{"type":"integer"}}')



### Functions used by this Node
transmit = Event('Transmit', {'schema': {'type': 'string'}, 'group': 'Communication'})
def send_udp_string(msg):
  #open socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    sock.sendto(msg, (param_ipAddress, param_port))
    transmit.emit(msg.strip('\n'))
  except socket.error, msg:
    print "error: %s\n" % msg
    local_event_Error.emit(msg)
  finally:
    if sock:
      sock.close()

def udp_ready():
    print 'UDP receiver started.'
    
def receive_udp_string(source, data):
	msg = str(data)
	handle_message(msg)

# Create a new UDP object to receive input from target destination
udp_receiver = UDP(source=None, dest=None, ready=udp_ready, received=receive_udp_string)



### Events used by this Node
response = Event('Receive', {'schema': {'type': 'string'}, 'group': 'Communication'})
def handle_message(received):
  response.emit(received)

  # List of expected strings to be received, and their relevant events
  event_list = {  "EndGramophone"   : local_event_EndGramophone,
                  "EndRadio"        : local_event_EndRadio,
                  "EndSymphonium"   : local_event_EndSymphonium  }

  # Strip received UDP command of whitespace
  received = received.strip()

  # Emit action based on relevant entry in the list of events
  if received in event_list:
    event_list[received].emit()



### Local actions this Node provides
def local_action_Start(arg = None):
  """{"title":"Start","desc":"Start","group":"General"}"""
  print 'Action Start requested'
  send_udp_string('start\n')

def local_action_SetLogging(arg = None):
  """{"title":"Set logging","desc":"Set logging level.","schema":{"title":"Level","type":"string","enum":["file","normal"],"required":"true"},"group":"General"}"""
  print 'Action SetLogging requested - '+arg
  send_udp_string('logging:'+arg+'\n')



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"An error has occured while communicating with the device.","group":"General"}')
# local_event_Error.emit(arg)



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'

  # bind receiver to localhost 
  udp_receiver.source = socket.gethostbyname(socket.gethostname()) + ':' + str(param_port)
