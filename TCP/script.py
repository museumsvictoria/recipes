'''TCP Node'''

### Libraries required by this Node
import socket



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address","desc":"The IP address","schema":{"type":"string"}}')
param_port = Parameter('{"title":"Port","schema":{"type":"integer"}}')



### Functions used by this Node
def send_tcp_string(msg):
  #open socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  sock.settimeout(10)
  try:
    sock.connect((param_ipAddress, param_port))
    sock.send(msg)
    data = sock.recv(1024)
    assert msg in data
  except socket.error, e:
    print "socket error: %s\n" % e
    local_event_Error.emit(e)
  except AssertionError, e:
    print "command error: %s\n" % e
    local_event_Error.emit(e)
  finally:
    sock.close()



### Local actions this Node provides
def local_action_Start(arg = None):
  """{"title":"Start","desc":"Start","group":"General"}"""
  print 'Action Start requested'
  send_tcp_string('start\n')

def local_action_Stop(arg = None):
  """{"title":"Stop","desc":"Stop","group":"General"}"""
  print 'Action Stop requested'
  send_tcp_string('stop\n')

def local_action_SetLogging(arg = None):
  """{"title":"SetLogging","desc":"SetLogging","schema":{"title":"Level","type":"string","enum":["file","normal"],"required":"true"},"group":"General"}"""
  print 'Action SetLogging requested - '+arg
  send_tcp_string('logging:'+arg+'\n')

def local_action_SetVolume(arg = None):
  """{"title":"SetVolume","desc":"SetVolume","schema":{"title":"Level","type":"integer","required":"true"},"group":"General"}"""
  print 'Action SetVolume requested - '+str(arg)
  send_tcp_string('volume:'+str(arg)+'\n')



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"An error has occured while communicating with the device.","group":"General"}')
# local_event_Error.emit(arg)



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'

