## Nodel Process Recipe 1.3 (January 2016)
> Dynamic exhibit node, designed to remove scripting requirements from the majority of simple exhibit nodes.

### Changes
**1.2.3 - 1.3**
- Parameter validation on first-use.
- Reset signals on node startup.
- Multi-power support.

### Setup
- **Lockout Percentage** refers to percentage of video elapsed before a received trigger can alter the playlist.
- **Button/Trigger Count** refers to amount of events tied to sensors or buttons.
- **Button Lights** provides the logic for turning on and off relays at the beginning and end of content playback.
- **Remote Power** provides the ability to specify your power requirements, and generate actions as a result.

### Requirements
- [Nodel] v2.1.1-release198

 [Nodel]: <https://github.com/museumvictoria/nodel.git>