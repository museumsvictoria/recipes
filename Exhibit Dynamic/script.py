# Date:         2017.11.23
# Version:      1.3

'''Dynamic Exhibit Node'''

### Libraries required by this Node
from time import sleep


### Parameters used by this Node
param_lockout = Parameter({"title":"Lockout Percentage","schema":{"type":"integer","format":"range","min": 0, "max": 100,"required":"true"},"order": next_seq()})
param_setupButtons = Parameter({"title":"Button/Trigger Count","schema":{"type":"integer","format":"range","min": 0, "max": 4,"required":"true"},"order": next_seq()})
param_setupLights = Parameter({"title":"Button Lights","schema":{"type":"boolean"},"group":"Setup","order": next_seq()})
param_setupPower = Parameter({"title":"Remote Power","schema":{"type":"integer","format":"range","min": 0, "max": 4,"required":"true"},"group":"Setup","order": next_seq()})

position = 0 # position of relevant video
trigger_tally = 0 # amount of attempted triggers



### Functions used by this Node
def reset_signals():
  all_buttons_off()
  local_event_Clip.emit(None)
  local_event_Index.emit(None)

def pad_number(num):
  padded = str(num).zfill(2)
  return padded

def update_buttons(button):
  # Turn off irrelevant lights.
  for num in range(1, param_setupButtons + 1):
    if (button != num):
      event = lookup_local_event('Button%s' % pad_number(num))
      if event:
        if event.arg is True:
          event.emit(False)
  # Turn on valid light.
  event = lookup_local_event('Button%s' % button)
  if event:
    if event.arg is not True:
      event.emit(True)

def all_buttons_off():
  # Turn off all buttons, regardless of the consequences.
  for num in range(1, param_setupButtons + 1):
    event = lookup_local_event('Button%s' % pad_number(num))
    if event:
      if event.arg is True:
        event.emit(False)

def play_conditions():
  global position
  clip = lookup_local_event('Index')
  if (clip.arg == 1): # first clip
    return True
  elif (clip.arg is None): # no clip tracked
    return True
  elif (position > param_lockout): # not a teaser, but lockout requirements met
    return True
  else:
    return False

def is_wired():
  # Wired or ResolutionFailure
  state = remote_action_PlayClip.getBindingState()
  if str(state) == 'ResolutionFailure':
    reset_signals()
    return False 
  elif str(state) == 'Wired':
    return True

def play_clip(num):
  remote_action_PlayClip.call(num)

### Local actions this Node provides
def local_action_Enable(arg = None):
  """{"title":"Enable","desc":"Enable","group":"General"}"""
  print 'Action Enable requested.'
  reset_signals()
  for num in range(1, param_setupPower + 1):
    action = lookup_local_action('PowerOnRP%s' % pad_number(num)) 
    if action:
      action.call()
  sleep(5)
  remote_action_PowerOnPC.call()

def local_action_Disable(arg = None):
  """{"title":"Disable","desc":"Disable","group":"General"}"""
  print 'Action Disable requested.'
  reset_signals()
  remote_action_PowerOffPC.call()
  sleep(20)
  for num in range(1, param_setupPower + 1):
    action = lookup_local_action('PowerOffRP%s' % pad_number(num)) 
    if action:
      action.call()

def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn","group":"Volume"}"""
  print 'Action MuteOn requested.'
  remote_action_MuteOnPC.call()

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff","group":"Volume"}"""
  print 'Action MuteOff requested.'
  remote_action_MuteOffPC.call()

def local_action_PowerOnPC(arg = None):
   """{"title":"PowerOnPC","group":"Computer"}"""
   print 'Action PowerOnPC requested.'
   remote_action_PowerOnPC.call()

def local_action_PowerOffPC(arg = None):
   """{"title":"PowerOffPC","group":"Computer"}"""
   print 'Action PowerOffPC requested.'
   remote_action_PowerOffPC.call()

def local_action_RestartPC(arg = None):
   """{"title":"RestartPC","group":"Computer"}"""
   print 'Action RestartPC requested.'
   remote_action_RestartPC.call()

# Dynamically create local button simulators
def createButtonAction(num):
  meta = {"title":"Trigger%s" % num,"group":"Simulate","order":1}

  def handler(arg):
      action = lookup_remote_action('SimulateTrigger%s' % num)
      if action:
        action.call()
      event = lookup_local_event('ButtonLight%s' % num)
      if event:
        event.call(True)

  action = Action('Press%s' % num, handler, metadata=meta)

# Dynamically create local power actions
def createLocalPower(num):

  def handler_on(arg):
    action = lookup_remote_action('PowerOnRP%s' % num)
    if action:
      action.call()

  def handler_off(arg):
    action = lookup_remote_action('PowerOffRP%s' % num)
    if action:
      action.call()

  action_local_on = Action('PowerOnRP%s' % num, handler_on, {'group':'Power'})
  action_local_off = Action('PowerOffRP%s' % num, handler_off, {'group': 'Power'})


  
### Remote actions this Node requires
remote_action_MuteOffPC = RemoteAction()
remote_action_MuteOnPC = RemoteAction()

remote_action_PlayClip = RemoteAction()

remote_action_PowerOnPC = RemoteAction()
remote_action_PowerOffPC = RemoteAction()
remote_action_RestartPC = RemoteAction()

def createRemotePower(num):
  remote_power_on = create_remote_action('PowerOnRP%s' % num)
  remote_power_off = create_remote_action('PowerOffRP%s' % num)

def createRemoteAction(num):
  remote_button = create_remote_action('SimulateTrigger%s' % num)

  if param_setupLights:
    remote_light = create_remote_action('ButtonLight%s' % num)



### Local events this Node provides
local_event_Clip = LocalEvent({"title":"Playing","schema":{"type":"string"},"group":"Playlist","order": next_seq()})
local_event_Index = LocalEvent({"title":"Index","schema":{"type":"integer"},"group":"Playlist","order": next_seq()})

def createButtonLightEvents(num):
  meta = {'group': "Button Lights", 'order': next_seq(), 'schema': {'type': 'boolean'}}

  event = Event('Button%s' % num, metadata=meta)

  action = lookup_remote_action('ButtonLight%s' % num.zfill(2))
  
  event.addEmitHandler(lambda arg: action.call(True) if arg == True else action.call(False))



### Remote events this Node requires
def remote_event_Enable(arg = None):
  """{"title":"Enable","desc":"Enable"}"""
  print 'Remote event Enable arrived.'
  local_action_Enable()

def remote_event_Disable(arg = None):
  """{"title":"Disable","desc":"Disable"}"""
  print 'Remote event Disable arrived.'
  local_action_Disable()

def remote_event_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn"}"""
  print 'Remote event MuteOn arrived.'
  local_action_MuteOn()

def remote_event_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff"}"""
  print 'Remote event MuteOff arrived.'
  local_action_MuteOff()

# Create remote events for triggers.
def createRemoteTrigger(num):

  meta = {"title":"Triggered%s" % num}

  def handler(arg):
    global trigger_tally
    if arg:
      if is_wired():
        if play_conditions():
          console.info('Trigger%s accepted.' % num)
          trigger_tally = 0
          play_clip(int(num) + 1)
          if param_setupLights:
            update_buttons(num)
        else:
          trigger_tally += 1
          console.warn('Trigger%s declined! (%s)' % (num, trigger_tally))
      else:
        console.error('Target node not wired.')

  event = create_remote_event('Triggered%s' % num, handler, metadata=meta)

  
# Monitor clip position to provide lockouts.
def remote_event_Position(arg = None):
  global position
  position = arg

# Monitor clip name.
def remote_event_Clip(arg = None):
  local_event_Clip.emit(str(arg))

# Monitor clip index.
def remote_event_Index(arg = None):
  local_event_Index.emit(arg)

  if arg == 1:
    all_buttons_off() # clears all buttons on first clip
  


### Main
def main(arg = None):

  # Ensure parameters are set.
  if ( (param_setupButtons is None) or (param_setupPower is None) ):
    console.warn('Parameters not set.')
    return

  # Create buttons.
  if param_setupButtons:
    for num in range(1, param_setupButtons + 1):

      num = pad_number(num)

      createButtonAction(num)
      createRemoteAction(num)
      createRemoteTrigger(num)
      if param_setupLights:
        createButtonLightEvents(num)

  # Create power.
  if param_setupPower:
    for num in range(1, param_setupPower + 1):   

      num = pad_number(num)

      createRemotePower(num)
      createLocalPower(num)

  # Clean up.
  reset_signals()