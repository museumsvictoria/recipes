'''Kramer VP-443 Node'''

### Libraries required by this Node
import socket
import re
import asyncore
from time import sleep
import threading
import atexit



### Parameters used by this Node
PORT = 10001
param_ipAddress = Parameter('{"title":"IP Address","desc":"The IP address","schema":{"type":"string","required":true}}')
input_list = ["CV1","CV2","COMP1","COMP2","PC1","PC2","HDMI1","HDMI2","HDMI3","HDMI4"]



### Functions used by this Node
client = None
loop_thread = threading.Thread(target=asyncore.loop, name="Asyncore Loop", args=(1,False))

@atexit.register
def cleanup():
  global client
  print 'shutdown'
  client.shutdown=True
  client.close()

class TCPClient(asyncore.dispatcher_with_send):
  def __init__(self, host, port):
    asyncore.dispatcher.__init__(self)
    self.host = host
    self.port = port
    self.out_buffer = ""
    self.in_buffer = ""
    self.retry = 1
    self.shutdown = False
    self.connected = False
    self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
    self.ping = PingThread(self)
    self.ping.start()
    print "initialising connection with the switcher."
    self.connect((self.host,self.port))
  def sendCommand(self, command):
    self.out_buffer += command
  def handle_connect(self):
    print "connected"
    self.retry = 1
    self.connected = True
  def handle_expt(self):
    if not(self.shutdown):
      print "unable to connect"
      local_event_Error.emit('unable to connect')
      self.reconnect()
  def handle_error(self):
    if not (self.shutdown):
      print "python error"
      local_event_Error.emit('python error')
      self.reconnect()
  def handle_close(self):
    print "connection closed"
    if not (self.shutdown):
      local_event_Error.emit('connection closed')
      self.reconnect()
  def reconnect(self):
    self.connected = False
    self.close()
    sleep(self.retry)
    self.retry = 120 if self.retry > 120 else self.retry * 2
    self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
    print "initialising connection with the switcher."
    self.connect((self.host,self.port))
  def handle_read(self):
    newdata = self.recv(8192)
    #if newdata == "": print "no data"
    self.in_buffer += newdata
    while True:
      new_buffer = self.in_buffer.split('\r',1)
      if len(new_buffer) > 1:
        if(new_buffer[0]!=""):
          handleResult(new_buffer[0])
        self.in_buffer = new_buffer[1]
      else: break
  def recv(self, buffer_size):
    try:
      data = self.socket.recv(buffer_size)
      return data
    except socket.error, why:
      # winsock sometimes throws ENOTCONN
      if why.args[0] in [ECONNRESET, ENOTCONN, ESHUTDOWN, ECONNABORTED]:
        self.handle_close()
        return ''
      else:
        raise

class PingThread(threading.Thread):
  def __init__(self, client):
    super(PingThread,self).__init__()
    self.client = client
  def run(self):
    counter = 0
    while self.client.shutdown == False:
      sleep(1)
      if self.client.connected == True:
        counter += 1
        if counter == 10:
          #print "ping"
          self.client.sendCommand("\r")
          counter = 0

def handleResult(result):
  match_input = re.search(r"Z\s(?:3|4)\s0\s(\d{1,2})", result, re.MULTILINE | re.VERBOSE)
  if match_input:
    print 'Current input: '+str(match_input.group(1))
    local_event_CurrentInput.emit(input_list[int(match_input.group(1))-1])
  match_freeze = re.search(r"Z\s6\s1\s(\d{0,1})", result, re.MULTILINE | re.VERBOSE)
  if match_freeze:
    print 'Freeze state: '+str(match_freeze.group(1))
    if(match_freeze.group(1)=="0"): local_event_FreezeOff.emit()
    if(match_freeze.group(1)=="1"): local_event_FreezeOn.emit()
  match_blank = re.search(r"Z\s6\s2\s(\d{0,1})", result, re.MULTILINE | re.VERBOSE)
  if match_blank:
    print 'Blanking state: '+str(match_blank.group(1))
    if(match_blank.group(1)=="0"): local_event_BlankOff.emit()
    if(match_blank.group(1)=="1"): local_event_BlankOn.emit()
  match_mute = re.search(r"Z\s6\s3\s(\d{0,1})", result, re.MULTILINE | re.VERBOSE)
  if match_mute:
    print 'Mute state: '+str(match_mute.group(1))
    if(match_mute.group(1)=="0"): local_event_MuteOff.emit()
    if(match_mute.group(1)=="1"): local_event_MuteOn.emit()



### Local actions this Node provides
def local_action_SetCV1(arg = None):
  '''{"title":"SetCV1","desc":"SetCV1","group":"Input - Composite Video"}'''
  print 'Action SetCV1 requested.'
  client.sendCommand('Y 3 0 1\r')

def local_action_SetCV2(arg = None):
  '''{"title":"SetCV2","desc":"SetCV2","group":"Input - Composite Video"}'''
  print 'Action SetCV2 requested.'
  client.sendCommand('Y 3 0 2\r')

def local_action_SetCOMP1(arg = None):
  '''{"title":"SetCOMP1","desc":"SetCOMP1","group":"Input - Component"}'''
  print 'Action SetCOMP1 requested.'
  client.sendCommand('Y 3 0 3\r')

def local_action_SetCOMP2(arg = None):
  '''{"title":"SetCOMP2","desc":"SetCOMP2","group":"Input - Component"}'''
  print 'Action SetCOMP2 requested.'
  client.sendCommand('Y 3 0 4\r')

def local_action_SetPC1(arg = None):
  '''{"title":"SetPC1","desc":"SetPC1","group":"Input - Computer"}'''
  print 'Action SetPC1 requested.'
  client.sendCommand('Y 3 0 5\r')

def local_action_SetPC2(arg = None):
  '''{"title":"SetPC2","desc":"SetPC2","group":"Input - Computer"}'''
  print 'Action SetPC2 requested.'
  client.sendCommand('Y 3 0 6\r')

def local_action_SetHDMI1(arg = None):
  '''{"title":"SetHDMI1","desc":"SetHDMI1","group":"Input - HDMI"}'''
  print 'Action SetHDMI1 requested.'
  client.sendCommand('Y 3 0 7\r')

def local_action_SetHDMI2(arg = None):
  '''{"title":"SetHDMI2","desc":"SetHDMI2","group":"Input - HDMI"}'''
  print 'Action SetHDMI2 requested.'
  client.sendCommand('Y 3 0 8\r')

def local_action_SetHDMI3(arg = None):
  '''{"title":"SetHDMI3","desc":"SetHDMI3","group":"Input - HDMI"}'''
  print 'Action SetHDMI3 requested.'
  client.sendCommand('Y 3 0 9\r')

def local_action_SetHDMI4(arg = None):
  '''{"title":"SetHDMI4","desc":"SetHDMI4","group":"Input - HDMI"}'''
  print 'Action SetHDMI4 requested.'
  client.sendCommand('Y 3 0 10\r')

def local_action_GetInput(arg = None):
  '''{"title":"GetInput","desc":"GetInput","group":"General"}'''
  print 'Action GetInput requested.'
  client.sendCommand('Y 4 0\r')

def local_action_FreezeOn(arg = None):
  '''{"title":"FreezeOn","desc":"FreezeOn","group":"General"}}'''
  print 'Action FreezeOn requested.'
  client.sendCommand('Y 6 1 1\r')

def local_action_FreezeOff(arg = None):
  '''{"title":"FreezeOff","desc":"FreezeOff","group":"General"}}'''
  print 'Action FreezeOff requested.'
  client.sendCommand('Y 6 1 0\r')

def local_action_BlankOn(arg = None):
  '''{"title":"BlankOn","desc":"BlankOn","group":"General"}}'''
  print 'Action BlankOn requested.'
  client.sendCommand('Y 6 2 1\r')

def local_action_BlankOff(arg = None):
  '''{"title":"BlankOff","desc":"BlankOff","group":"General"}}'''
  print 'Action BlankOff requested.'
  client.sendCommand('Y 6 2 0\r')

def local_action_MuteOn(arg = None):
  '''{"title":"MuteOn","desc":"MuteOn","group":"General"}}'''
  print 'Action MuteOn requested.'
  client.sendCommand('Y 6 3 1\r')

def local_action_MuteOff(arg = None):
  '''{"title":"MuteOff","desc":"MuteOff","group":"General"}}'''
  print 'Action MuteOff requested.'
  client.sendCommand('Y 6 3 0\r')



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"Error","group":"General"}')
local_event_CurrentInput = LocalEvent('{"title":"CurrentInput","desc":"CurrentInput","group":"General","schema":{"title":"Input","type":"string"}}')
local_event_FreezeOn = LocalEvent('{"title":"FreezeOn","desc":"FreezeOn","group":"General"}')
local_event_FreezeOff = LocalEvent('{"title":"FreezeOff","desc":"FreezeOff","group":"General"}')
local_event_BlankOn = LocalEvent('{"title":"BlankOn","desc":"BlankOn","group":"General"}')
local_event_BlankOff = LocalEvent('{"title":"BlankOff","desc":"BlankOff","group":"General"}')
local_event_MuteOn = LocalEvent('{"title":"MuteOn","desc":"MuteOn","group":"General"}')
local_event_MuteOff = LocalEvent('{"title":"MuteOff","desc":"MuteOff","group":"General"}')



### Main
def main(arg = None):
  global client
  if(param_ipAddress and PORT):
    client = TCPClient(param_ipAddress, PORT)
    loop_thread.start()
  else:
    local_event_Error.emit('No IP address or port specified')
    print 'No IP address or port specified'
  print 'Nodel script started.'