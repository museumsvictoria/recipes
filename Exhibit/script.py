'''Exhibit Node'''

### Libraries required by this Node
from time import sleep



### Local actions this Node provides

def local_action_Enable(arg = None):
  """{"title":"Enable","desc":"Enable","group":"General"}"""
  print 'Action Enable requested.'
  remote_action_PowerOnPC01.call()
  remote_action_PowerOnPJ01.call()
  ### Uncomment the below two lines if PC is a Mac running OSX.
  ### sleep(10)
  ### remote_action_RestartPC01.call()

def local_action_Disable(arg = None):
  """{"title":"Disable","desc":"Disable","group":"General"}"""
  print 'Action Disable requested.'
  ##remote_action_PowerOffRP01.call()
  ##remote_action_PowerOffRP02.call()
  remote_action_PowerOffPC01.call()
  remote_action_PowerOffPJ01.call()
  
def local_action_RestartPC(arg = None):
  """{"title":"RestartPC","desc":"Restart","group":"PC Control"}"""
  print 'Action Restart requested.'
  remote_action_RestartPC01.call()

def local_action_PowerOffPC(arg = None):
  """{"title":"PowerOffPC","desc":"PowerOff","group":"PC Control"}"""
  print 'Action PowerOff requested.'
  remote_action_PowerOffPC01.call()

def local_action_PowerOnPC(arg = None):
  """{"title":"PowerOnPC","desc":"PowerOn","group":"PC Control"}"""
  print 'Action PowerOn requested.'
  remote_action_PowerOnPC01.call()

def local_action_PowerOffPJ01(arg = None):
  """{"title":"PowerOffPJ01","desc":"PowerOff","group":"Projector"}"""
  print 'Action PowerOff requested.'
  remote_action_PowerOffPJ01.call()

def local_action_PowerOnPJ01(arg = None):
  """{"title":"PowerOnPJ01","desc":"PowerOn","group":"Projector"}"""
  print 'Action PowerOn requested.'
  remote_action_PowerOnPJ01.call()
  
def local_action_MuteOffPJ(arg = None):
  """{"title":"MuteOffPJ","desc":"MuteOff","group":"Projector"}"""
  print 'Action MuteOff requested.'
  remote_action_MuteOffPJ01.call()

def local_action_MuteOnPJ(arg = None):
  """{"title":"MuteOnPJ","desc":"MuteOn","group":"Projector"}"""
  print 'Action MuteOn requested.'
  remote_action_MuteOnPJ01.call()
  
def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn","group":"Audio"}"""
  print 'Action MuteOn requested.'
  remote_action_MuteOn.call()

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff","group":"Audio"}"""
  print 'Action MuteOff requested.'
  remote_action_MuteOff.call()

def local_action_PowerOn(arg = None):
  """{"title":"PowerOn","desc":"PowerOn","group":"Power"}"""
  print 'Action PowerOn requested.'
  remote_action_PowerOnRP01.call()

def local_action_PowerOff(arg = None):
  """{"title":"PowerOff","desc":"PowerOff","group":"Power","caution":"Are you sure?"}"""
  print 'Action PowerOff requested.'
  remote_action_PowerOffRP01.call()


### Remote actions this Node requires
remote_action_PowerOnPJ01 = RemoteAction()
remote_action_PowerOffPJ01 = RemoteAction()
remote_action_MuteOnPJ01 = RemoteAction()
remote_action_MuteOffPJ01 = RemoteAction()

remote_action_PowerOnRP01 = RemoteAction()
remote_action_PowerOffRP01 = RemoteAction()

remote_action_PowerOnPC01 = RemoteAction()
remote_action_PowerOffPC01 = RemoteAction()
remote_action_RestartPC01 = RemoteAction()

remote_action_MuteOn = RemoteAction()
remote_action_MuteOff = RemoteAction()


### Remote events this Node requires
def remote_event_Enable(arg = None):
  """{"title":"Enable","desc":"Enable","group":"General"}"""
  print 'Remote event Enable arrived.'
  local_action_Enable()

def remote_event_Disable(arg = None):
  """{"title":"Disable","desc":"Disable","group":"General"}"""
  print 'Remote event Disable arrived.'
  local_action_Disable()

def remote_event_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn"}"""
  print 'Remote event MuteOn arrived.'
  local_action_MuteOn()

def remote_event_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff"}"""
  print 'Remote event MuteOff arrived.'
  local_action_MuteOff()
  


### Status extension this Node requires
from status import *

@after_main
def extendNode():

  if lookup_local_event('Desired Power'):
    lookup_local_action('Enable').addCallHandler(lambda arg: lookup_local_event('Desired Power').emit('On'))
    lookup_local_action('Disable').addCallHandler(lambda arg: lookup_local_event('Desired Power').emit('Off'))

  def handlePower(arg):
    if arg =='On':
      lookup_local_action('Enable').call()
    elif arg =='Off':
      lookup_local_action('Disable').call()

  powerAction = lookup_local_action('Power')
  if powerAction:
    powerAction.addCallHandler(handlePower) 
    


### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'