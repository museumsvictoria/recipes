'''Exhibit Node'''

### Libraries required by this Node
from time import sleep



### Local actions this Node provides
def local_action_Enable(arg = None):
  """{"title":"Enable","desc":"Enable"}"""
  print 'Action Enable requested.'
  remote_action_PowerOnRP.call()
  sleep(5)
  remote_action_PowerOnPC.call()

def local_action_Disable(arg = None):
  """{"title":"Disable","desc":"Disable"}"""
  print 'Action Disable requested.'
  remote_action_PowerOffPC.call()
  sleep(40)
  remote_action_PowerOffRP.call()



### Remote actions this Node requires
remote_action_PowerOnPC = RemoteAction()
remote_action_PowerOffPC = RemoteAction()

remote_action_PowerOnRP = RemoteAction()
remote_action_PowerOffRP = RemoteAction()



### Remote events this Node requires
def remote_event_Enable(arg = None):
  """{"title":"Enable","desc":"Enable","group":"General"}"""
  print 'Remote event Enable arrived.'
  local_action_Enable()

def remote_event_Disable(arg = None):
  """{"title":"Disable","desc":"Disable","group":"General"}"""
  print 'Remote event Disable arrived.'
  local_action_Disable()

def remote_event_ControlEnable(arg = None):
  """{"title":"ControlEnable","desc":"ControlEnable","group":"General"}"""
  print 'Remote event ControlEnable arrived.'
  local_action_Enable()

def remote_event_ControlRestart(arg = None):
  """{"title":"ControlRestart","desc":"ControlRestart","group":"General"}"""
  print 'Remote event ControlRestart arrived.'
  local_action_Disable()
  sleep(10)
  local_action_Enable()



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'