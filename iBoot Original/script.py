'''iBoot Original Node'''

### Libraries required by this Node
import socket
import struct



### Parameters used by this Node
PORT = 80
PASSWORD = "PASS"
param_ipAddress = Parameter('{"title":"IP address","schema":{"type":"string"}}')



### Functions used by this Node
def main():
  print 'started'

def cmd(control):
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10)
    s.connect((param_ipAddress, PORT))
    s.sendall('\x1b'+PASSWORD+'\x1b'+control+'\x0d')
    data = s.recv(16)
    if(data=="OFF"):
      local_event_PowerOff.emit()
    elif(data=="ON"):
      local_event_PowerOn.emit()
    else:
      raise Exception("Error: unexpected response")
  except Exception, e:
    local_event_Error.emit(e)
  finally:
    s.close()



### Local actions this Node provides
def local_action_PowerOn(arg):
  '''{"title":"PowerOn","desc":"PowerOn"}'''
  cmd("n")
  print 'Action PowerOn requested.'

def local_action_PowerOff(arg):
  '''{"title":"PowerOff","desc":"PowerOff"}'''
  cmd("f")
  print 'Action PowerOff requested.'

def local_action_GetPower(arg):
  '''{"title":"GetPower","desc":"GetPower"}'''
  cmd("q")
  print 'Action GetPower requested.'



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"Error","group":"General"}')
local_event_PowerOn = LocalEvent('{"title":"PowerOn","desc":"PowerOn","group":"General"}')
local_event_PowerOff = LocalEvent('{"title":"PowerOff","desc":"PowerOff","group":"General"}')



### Main
def main():
  # Start your script here.
  print 'Nodel script started.'