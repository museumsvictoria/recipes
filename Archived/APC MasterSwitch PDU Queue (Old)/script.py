'''APC MasterSwitch PDU Queue Node'''

### Libraries required by this Node
from Queue import *
import threading
import atexit



### Classes required by this Node
class TimerClass(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.event = threading.Event()
  def run(self):
    while not self.event.isSet():
      if queue.empty() != True:
        job = queue.get()
        try:
          print "Calling command " + job['function']
          func = globals()[job['function']]
          arg = job['args'] if 'args' in job else ''
          func.call(arg)
          self.event.wait(job['delay'])
          queue.task_done()
        except Exception, e:
          print e
          print "Failed to call command " + job['function']
      else:
        self.event.wait(1)
  def stop(self):
      self.event.set()

queue = Queue()
th = TimerClass()

@atexit.register
def cleanup():
  print 'shutdown'
  th.stop()



### Local actions this Node provides
def local_action_PowerOn1(x = None):
  '''{ "title": "PowerOn1", "desc": "PowerOn1", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn1', 'delay': 3})
  print 'Action PowerOn1 requested.'

def local_action_PowerOff1(x = None):
  '''{ "title": "PowerOff1", "desc": "PowerOff1", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff1', 'delay': 3})
  print 'Action PowerOff1 requested.'

def local_action_PowerOn2(x = None):
  '''{ "title": "PowerOn2", "desc": "PowerOn2", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn2', 'delay': 3})
  print 'Action PowerOn2 requested.'

def local_action_PowerOff2(x = None):
  '''{ "title": "PowerOff2", "desc": "PowerOff2", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff2', 'delay': 3})
  print 'Action PowerOff2 requested.'

def local_action_PowerOn3(x = None):
  '''{ "title": "PowerOn3", "desc": "PowerOn3", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn3', 'delay': 3})
  print 'Action PowerOn3 requested.'

def local_action_PowerOff3(x = None):
  '''{ "title": "PowerOff3", "desc": "PowerOff3", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff3', 'delay': 3})
  print 'Action PowerOff3 requested.'

def local_action_PowerOn4(x = None):
  '''{ "title": "PowerOn4", "desc": "PowerOn4", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn4', 'delay': 3})
  print 'Action PowerOn4 requested.'

def local_action_PowerOff4(x = None):
  '''{ "title": "PowerOff4", "desc": "PowerOff4", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff4', 'delay': 3})
  print 'Action PowerOff4 requested.'

def local_action_PowerOn5(x = None):
  '''{ "title": "PowerOn5", "desc": "PowerOn5", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn5', 'delay': 3})
  print 'Action PowerOn5 requested.'

def local_action_PowerOff5(x = None):
  '''{ "title": "PowerOff5", "desc": "PowerOff5", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff5', 'delay': 3})
  print 'Action PowerOff5 requested.'

def local_action_PowerOn6(x = None):
  '''{ "title": "PowerOn6", "desc": "PowerOn6", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn6', 'delay': 3})
  print 'Action PowerOn6 requested.'

def local_action_PowerOff6(x = None):
  '''{ "title": "PowerOff6", "desc": "PowerOff6", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff6', 'delay': 3})
  print 'Action PowerOff6 requested.'

def local_action_PowerOn7(x = None):
  '''{ "title": "PowerOn7", "desc": "PowerOn7", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn7', 'delay': 3})
  print 'Action PowerOn7 requested.'

def local_action_PowerOff7(x = None):
  '''{ "title": "PowerOff7", "desc": "PowerOff7", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff7', 'delay': 3})
  print 'Action PowerOff7 requested.'

def local_action_PowerOn8(x = None):
  '''{ "title": "PowerOn8", "desc": "PowerOn8", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn8', 'delay': 3})
  print 'Action PowerOn8 requested.'

def local_action_PowerOff8(x = None):
  '''{ "title": "PowerOff8", "desc": "PowerOff8", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff8', 'delay': 3})
  print 'Action PowerOff8 requested.'



### Remote actions this Node requires
remote_action_PowerOn1 = RemoteAction()
remote_action_PowerOff1 = RemoteAction()

remote_action_PowerOn2 = RemoteAction()
remote_action_PowerOff2 = RemoteAction()

remote_action_PowerOn3 = RemoteAction()
remote_action_PowerOff3 = RemoteAction()

remote_action_PowerOn4 = RemoteAction()
remote_action_PowerOff4 = RemoteAction()

remote_action_PowerOn5 = RemoteAction()
remote_action_PowerOff5 = RemoteAction()

remote_action_PowerOn6 = RemoteAction()
remote_action_PowerOff6 = RemoteAction()

remote_action_PowerOn7 = RemoteAction()
remote_action_PowerOff7 = RemoteAction()

remote_action_PowerOn8 = RemoteAction()
remote_action_PowerOff8 = RemoteAction()



### Main
def main():
  # Start your script here.
  th.start()
  print 'Nodel script started.'