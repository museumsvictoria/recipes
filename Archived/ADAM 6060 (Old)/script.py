'''ADAM 6060 Node'''

### Libraries required by this Node
from pymodbus.client.sync import ModbusTcpClient
import threading
import atexit



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address", "schema":{"type":"string"}}')
POLL = 100
# old firmware uses unit=0, new firmware unit=1. Change this if you are receiving a connection error
UNIT = 1



### Functions used by this Node
lock = threading.Lock()

class ModbusPoll(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.event = threading.Event()
    self.client = None
    self.current = [True,True,True,True,True,True]
  def run(self):
    self.client = ModbusTcpClient(param_ipAddress)
    while not self.event.isSet():
      lock.acquire()
      try:
        result = self.client.read_coils(0,6, unit=UNIT)
        for num in range(0,6):
          if (result.bits[num] != self.current[num]):
            func = globals()['local_event_Input'+str(num+1)+('On' if self.current[num] else 'Off')]
            func.emit()
          self.current[num] = result.bits[num]
        self.event.wait(POLL/1000.0)
      except AttributeError:
        local_event_Error.emit('Could not connect to ADAM')
        self.event.wait(1)
      except Exception, e:
        local_event_Error.emit(e)
        self.event.wait(1)
      finally:
        lock.release()
    self.client.close()
  def on(self, num):
    if not self.event.isSet():
      lock.acquire()
      try:
        self.client.write_coil(num, True, unit=UNIT)
      except Exception, e:
        local_event_Error.emit(e)
      finally:
        lock.release()
  def off(self, num):
    if not self.event.isSet():
      lock.acquire()
      try:
        self.client.write_coil(num, False, unit=UNIT)
      except Exception, e:
        local_event_Error.emit(e)
      finally:
        lock.release()
  def stop(self):
    self.event.set()

th = ModbusPoll()

@atexit.register
def cleanup():
  print 'shutdown'
  th.stop()



### Local actions this Node provides
def local_action_Relay1On(arg):
  '''{"title":"Relay1On","desc":"Relay1On", "group":"Relays"}'''
  th.on(16)
  print 'Action Relay1On requested.'

def local_action_Relay1Off(arg):
  '''{"title":"Relay1Off","desc":"Relay1Off", "group":"Relays"}'''
  th.off(16)
  print 'Action Relay1Off requested.'

def local_action_Relay2On(arg):
  '''{"title":"Relay2On","desc":"Relay2On", "group":"Relays"}'''
  th.on(17)
  print 'Action Relay2On requested.'

def local_action_Relay2Off(arg):
  '''{"title":"Relay2Off","desc":"Relay2Off", "group":"Relays"}'''
  th.off(17)
  print 'Action Relay2Off requested.'

def local_action_Relay3On(arg):
  '''{"title":"Relay3On","desc":"Relay3On", "group":"Relays"}'''
  th.on(18)
  print 'Action Relay3On requested.'

def local_action_Relay3Off(arg):
  '''{"title":"Relay3Off","desc":"Relay3Off", "group":"Relays"}'''
  th.off(18)
  print 'Action Relay3Off requested.'

def local_action_Relay4On(arg):
  '''{"title":"Relay4On","desc":"Relay4On", "group":"Relays"}'''
  th.on(19)
  print 'Action Relay4On requested.'

def local_action_Relay4Off(arg):
  '''{"title":"Relay4Off","desc":"Relay4Off", "group":"Relays"}'''
  th.off(19)
  print 'Action Relay4Off requested.'

def local_action_Relay5On(arg):
  '''{"title":"Relay5On","desc":"Relay5On", "group":"Relays"}'''
  th.on(20)
  print 'Action Relay5On requested.'

def local_action_Relay5Off(arg):
  '''{"title":"Relay5Off","desc":"Relay5Off", "group":"Relays"}'''
  th.off(20)
  print 'Action Relay5Off requested.'

def local_action_Relay6On(arg):
  '''{"title":"Relay6On","desc":"Relay6On", "group":"Relays"}'''
  th.on(21)
  print 'Action Relay6On requested.'

def local_action_Relay6Off(arg):
  '''{"title":"Relay6Off","desc":"Relay6Off", "group":"Relays"}'''
  th.off(21)
  print 'Action Relay6Off requested.'



### Local events this Node provides
local_event_Input1On = LocalEvent('{"title":"Input1On","desc":"Input1On", "group":"Inputs"}')
local_event_Input2On = LocalEvent('{"title":"Input2On","desc":"Input2On", "group":"Inputs"}')
local_event_Input3On = LocalEvent('{"title":"Input3On","desc":"Input3On", "group":"Inputs"}')
local_event_Input4On = LocalEvent('{"title":"Input4On","desc":"Input4On", "group":"Inputs"}')
local_event_Input5On = LocalEvent('{"title":"Input5On","desc":"Input5On", "group":"Inputs"}')
local_event_Input6On = LocalEvent('{"title":"Input6On","desc":"Input6On", "group":"Inputs"}')
local_event_Input1Off = LocalEvent('{"title":"Input1Off","desc":"Input1Off", "group":"Inputs"}')
local_event_Input2Off = LocalEvent('{"title":"Input2Off","desc":"Input2Off", "group":"Inputs"}')
local_event_Input3Off = LocalEvent('{"title":"Input3Off","desc":"Input3Off", "group":"Inputs"}')
local_event_Input4Off = LocalEvent('{"title":"Input4Off","desc":"Input4Off", "group":"Inputs"}')
local_event_Input5Off = LocalEvent('{"title":"Input5Off","desc":"Input5Off", "group":"Inputs"}')
local_event_Input6Off = LocalEvent('{"title":"Input6Off","desc":"Input6Off", "group":"Inputs"}')
local_event_Error = LocalEvent('{"title":"Error","desc":"Error", "group":"General"}')



### Main
def main():
  # Start your script here.
  if(param_ipAddress):
    th.start()
  else:
    local_event_Error.emit('configuration not set')
    print 'configuration not set'