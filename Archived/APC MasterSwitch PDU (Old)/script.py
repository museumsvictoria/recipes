'''APC MasterSwitch PDU Node'''

### Libraries required by this Node
import socket
import re
from time import sleep



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address","schema":{"type":"string"}}')
ON = 1
OFF = 2
OID = "2b06010401823e01010404020103"
SET_PACKET_HEAD = "3030020100040770726976617465a3220202000102010002010030163014060f" + OID
GET_PACKET_HEAD = "302f020100040770726976617465a0210202000102010002010030153013060f" + OID
GET_TAIL = "0500"
VAR_INT = "0201"
UDP_PORT = 161



### Functions used by this Node
def set_status(outlet, state):
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    sock.settimeout(3)
    sock.bind(('', 0))
    packet = SET_PACKET_HEAD + '%02x' % outlet + VAR_INT + '%02x' % state
    sock.sendto(packet.decode('hex'), (param_ipAddress, UDP_PORT))
    data, addr = sock.recvfrom(1024)
    buffer = data
    while(not read_result(buffer)):
      data, addr = sock.recvfrom(1024)
      buffer+=data
  except:
    print 'error setting status for outlet %02x' % outlet
    local_event_Error.emit('error setting status for outlet %02x' % outlet)
  finally:
    if sock:
      sock.close()

def get_status(outlet):
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    sock.settimeout(3)
    sock.bind(('', 0))
    packet = GET_PACKET_HEAD + '%02x' % outlet + GET_TAIL
    sock.sendto(packet.decode('hex'), (param_ipAddress, UDP_PORT))
    data, addr = sock.recvfrom(1024)
    buffer = data
    while(not read_result(buffer)):
      data, addr = sock.recvfrom(1024)
      buffer+=data
  except:
    print 'error getting status for outlet %02x' % outlet
    local_event_Error.emit('error getting status for outlet %02x' % outlet)
  finally:
    if sock:
      sock.close()

def read_result(data):
  match = re.search(OID+r"(\d{2})"+VAR_INT+r"(\d{2})", data.encode('hex'), re.MULTILINE | re.VERBOSE)
  if match:
    globals()["local_event_Outlet"+str(int(match.group(1)))+('On' if match.group(2)=="01" else 'Off')].emit()
    return True
  else:
    return False



### Local actions this Node provides
def local_action_PowerOn1(arg = None):
  '''{ "title":"PowerOn1", "desc":"PowerOn1", "group":"Outlets" }'''
  print 'Action PowerOn1 requested.'
  set_status(1,ON)

def local_action_PowerOff1(arg = None):
  '''{ "title":"PowerOff1", "desc":"PowerOff1", "group":"Outlets" }'''
  print 'Action PowerOff1 requested.'
  set_status(1,OFF)

def local_action_PowerOn2(arg = None):
  '''{ "title":"PowerOn2", "desc":"PowerOn2", "group":"Outlets" }'''
  print 'Action PowerOn2 requested.'
  set_status(2,ON)

def local_action_PowerOff2(arg = None):
  '''{ "title":"PowerOff2", "desc":"PowerOff2", "group":"Outlets" }'''
  print 'Action PowerOff2 requested.'
  set_status(2,OFF)

def local_action_PowerOn3(arg = None):
  '''{ "title":"PowerOn3", "desc":"PowerOn3", "group":"Outlets" }'''
  print 'Action PowerOn3 requested.'
  set_status(3,ON)

def local_action_PowerOff3(arg = None):
  '''{ "title":"PowerOff3", "desc":"PowerOff3", "group":"Outlets" }'''
  print 'Action PowerOff3 requested.'
  set_status(3,OFF)

def local_action_PowerOn4(arg = None):
  '''{ "title":"PowerOn4", "desc":"PowerOn4", "group":"Outlets" }'''
  print 'Action PowerOn4 requested.'
  set_status(4,ON)

def local_action_PowerOff4(arg = None):
  '''{ "title":"PPowerOff4", "desc":"PowerOff4", "group":"Outlets" }'''
  print 'Action PowerOff4 requested.'
  set_status(4,OFF)

def local_action_PowerOn5(arg = None):
  '''{ "title":"PowerOn5", "desc":"PowerOn5", "group":"Outlets" }'''
  print 'Action PowerOn5 requested.'
  set_status(5,ON)

def local_action_PowerOff5(arg = None):
  '''{ "title":"PowerOff5", "desc":"PowerOff5", "group":"Outlets" }'''
  print 'Action PowerOff5 requested.'
  set_status(5,OFF)

def local_action_PowerOn6(arg = None):
  '''{ "title":"PowerOn6", "desc":"PowerOn6", "group":"Outlets" }'''
  print 'Action PowerOn6 requested.'
  set_status(6,ON)

def local_action_PowerOff6(arg = None):
  '''{ "title":"PowerOff6", "desc":"PowerOff6", "group":"Outlets" }'''
  print 'Action PowerOff6 requested.'
  set_status(6,OFF)

def local_action_PowerOn7(arg = None):
  '''{ "title":"PowerOn7", "desc":"PowerOn7", "group":"Outlets" }'''
  print 'Action PowerOn7 requested.'
  set_status(7,ON)

def local_action_PowerOff7(arg = None):
  '''{ "title":"PowerOff7", "desc":"PowerOff7", "group":"Outlets" }'''
  print 'Action PowerOff7 requested.'
  set_status(7,OFF)

def local_action_PowerOn8(arg = None):
  '''{ "title":"PowerOn8", "desc":"PowerOn8", "group":"Outlets" }'''
  print 'Action PowerOn8 requested.'
  set_status(8,ON)

def local_action_PowerOff8(arg = None):
  '''{ "title":"PowerOff8", "desc":"PowerOff8", "group":"Outlets" }'''
  print 'Action PowerOff8 requested.'
  set_status(8,OFF)

def local_action_PowerOnAll(arg = None):
  '''{ "title":"PowerOnAll", "desc":"PowerOnAll", "group":"General" }'''
  print 'Action PowerOnAll requested.'
  for i in range(1,9):
    set_status(i, ON)

def local_action_PowerOffAll(arg = None):
  '''{ "title":"PowerOffAll", "desc":"PowerOffAll", "group":"General" }'''
  print 'Action PowerOffAll requested.'
  for i in range(1,9):
    set_status(i, OFF)

def local_action_PowerOnAllDelayed(arg = None):
  '''{ "title":"PowerOnAllDelayed", "desc":"PowerOnAllDelayed", "group":"General" }'''
  print 'Action PowerOnAllDelayed requested.'
  for i in range(1,9):
    set_status(i, ON)
    sleep(3)

def local_action_PowerOffAllDelayed(arg = None):
  '''{ "title":"PowerOffAllDelayed", "desc":"PowerOffAllDelayed", "group":"General" }'''
  print 'Action PowerOffAllDelayed requested.'
  for i in range(1,9):
    set_status(i, OFF)
    sleep(3)

def local_action_GetAllStatus(arg = None):
  '''{ "title":"GetAllStatus", "desc":"GetAllStatus", "group":"General" }'''
  print 'Action GetAllStatus requested.'
  for i in range(1,9):
    get_status(i)



### Local events this Node provides
local_event_Outlet1On = LocalEvent('{ "title": "Outlet1On", "desc": "Outlet1On", "group": "Outlets" }')
local_event_Outlet1Off = LocalEvent('{ "title": "Outlet1Off", "desc": "Outlet1Off", "group": "Outlets" }')
local_event_Outlet2On = LocalEvent('{ "title": "Outlet2On", "desc": "Outlet2On", "group": "Outlets" }')
local_event_Outlet2Off = LocalEvent('{ "title": "Outlet2Off", "desc": "Outlet2Off", "group": "Outlets" }')
local_event_Outlet3On = LocalEvent('{ "title": "Outlet3On", "desc": "Outlet3On", "group": "Outlets" }')
local_event_Outlet3Off = LocalEvent('{ "title": "Outlet3Off", "desc": "Outlet3Off", "group": "Outlets" }')
local_event_Outlet4On = LocalEvent('{ "title": "Outlet4On", "desc": "Outlet4On", "group": "Outlets" }')
local_event_Outlet4Off = LocalEvent('{ "title": "Outlet4Off", "desc": "Outlet4Off", "group": "Outlets" }')
local_event_Outlet5On = LocalEvent('{ "title": "Outlet5On", "desc": "Outlet5On", "group": "Outlets" }')
local_event_Outlet5Off = LocalEvent('{ "title": "Outlet5Off", "desc": "Outlet5Off", "group": "Outlets" }')
local_event_Outlet6On = LocalEvent('{ "title": "Outlet6On", "desc": "Outlet6On", "group": "Outlets" }')
local_event_Outlet6Off = LocalEvent('{ "title": "Outlet6Off", "desc": "Outlet6Off", "group": "Outlets" }')
local_event_Outlet7On = LocalEvent('{ "title": "Outlet7On", "desc": "Outlet7On", "group": "Outlets" }')
local_event_Outlet7Off = LocalEvent('{ "title": "Outlet7Off", "desc": "Outlet7Off", "group": "Outlets" }')
local_event_Outlet8On = LocalEvent('{ "title": "Outlet8On", "desc": "Outlet8On", "group": "Outlets" }')
local_event_Outlet8Off = LocalEvent('{ "title": "Outlet8Off", "desc": "Outlet8Off", "group": "Outlets" }')
local_event_Error = LocalEvent('{ "title": "Error", "desc": "Error", "group": "General" }')



### Main
def main():
  # Start your script here.
  print 'Nodel script started.'