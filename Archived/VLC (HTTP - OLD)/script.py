# Copyright (c) 2016 Museum Victoria
# This software is released under the MIT license (see license.txt for details)

'''VLC Node - Process Management & Web Interface'''

### Libraries required by this Node
import urllib2
import base64



### Parameters used by this Node
app = 'C:/Program Files/VideoLAN/VLC/vlc.exe'
address = '127.0.0.1'
port = '1337'
base_url = ('http://' + address + ':' + port + '/requests/')
password = 'vlc'

current_clip = 'Teaser'
clip_position = 0.0
clip_lockout = .5 #50% Playback Progress
pl_offset = 5



### Process managed by this Node

# Useful arguments for VLC
# --no-keyboard-events
# --no-mouse-events
# --quiet  (turn off warning and information messages)
# --no-osd (disable on screen display)
# --qt-minimal-view (disable menu)
# --no-qt-fs-controller (disable gui controller)
# --fullscreen

Process([app, '--http-host=' + address, '--http-port=' + port, 'playlist.xspf',
  '--no-osd', '--quiet', '--qt-minimal-view', '--repeat', '--http-password=vlc'],
      lambda: console.info('Started!'),
      lambda arg: console.info('Got stdout line:' + arg),
      lambda arg: console.info(arg + ' was sent to process'),
      lambda arg: console.info('Got stderr line:' + arg),
      lambda arg: console.info('Stopped! exitCode:%s' % arg),
      lambda: console.warn('Request timeout!'))



### Functions used by this node
def authenticate(command):
  request = urllib2.Request(base_url + command)
  base64string = base64.encodestring('%s:%s' % ('', password)).replace('\n', '')
  request.add_header("Authorization", "Basic %s" % base64string)   
  return request

def progress():
  status = authenticate('status.json')
  status_json = json_decode(urllib2.urlopen(status).read())
  return status_json['position']

def monitor(clip):
  global clip_position, current_clip
  while clip_position != 1:
    if current_clip != clip:
      return current_clip
    clip_position = progress()

def update():
  global current_clip
  name = authenticate('status.json')
  name_json = json_decode(urllib2.urlopen(name).read())
  current_clip = name_json['information']['category']['meta']['filename']
  return current_clip

def set_volume(vol):
  urllib2.urlopen(authenticate('status.xml?command=volume&val=' + vol ))

def play_teaser(): #Playlist - Item 4
  global current_clip
  urllib2.urlopen(authenticate('status.xml?command=pl_play&id=' + str((0 + pl_offset))))
  current_clip = 'Teaser'

def play_clip(num):
  # Request playlist item.
  urllib2.urlopen(authenticate('status.xml?command=pl_play&id=' + str((num + pl_offset))))
  # Update playback information.
  clip = update()
  # Monitor clip and release when playback has ended.
  monitor(clip)
  if clip is current_clip:
    play_teaser()
  


# Local actions this Node provides
def local_action_PlayTeaser(arg = None):
  """{"title":"Teaser","desc":"Play Teaser.","group":"Playlist"}"""
  print 'Action PlayTeaser requested'
  play_teaser()

def local_action_PlayClip01(arg = None):
  """{"title":"Clip01","desc":"Play Clip01","group":"Playlist"}"""
  if (clip_position > clip_lockout) or (current_clip is 'Teaser'):
    print 'Action PlayClip01 requested'
    play_clip(1)

def local_action_PlayClip02(arg = None):
  """{"title":"Clip02","desc":"Play Clip02","group":"Playlist"}"""
  if (clip_position > clip_lockout) or (current_clip is 'Teaser'):
    print 'Action PlayClip02 requested'
    play_clip(2)

def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"Mute this computer.","group":"Volume"}"""
  print 'Action MuteOn requested'
  set_volume('0')

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"Un-mute this computer.","group":"Volume"}"""
  print 'Action MuteOff requested'
  set_volume('255')

def local_action_SetVolume(arg = None):
  """{"title":"SetVolume","desc":"Set volume.","schema":{"title":"Manual Level : 0 - 255","type":"integer","required":"true"},"group":"Volume"}"""
  print 'Action SetVolume requested - '+str(arg)
  if 0 <= arg <= 255:
    set_volume(str(arg))



def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'