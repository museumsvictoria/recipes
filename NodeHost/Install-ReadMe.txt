25th May 2017
LS

This readme briefly outline the nodes and files you need to edit to run a single Nodel Service which manages multiple Galleries via the Bootstrapper Node

All source files are located at:
\\mvdmsmarvin\nodel\recipes\nodehost

The Install-NodeHost.bat basically does the following:
- Checks for old nodelsvc or node-hostsvc services and stops then deletes.
- Checks for and creates Nodel folders then copies Host files from the above location to the Local PC. 
- Installs the nodel-hostsvc service and sets servive start and recovery options.
- Starts the nodel-host service.
- Creates the PC Node.

Once the Host is successfully installed, you need to do the following:
- Add your Bootstrapper Nodes to c:\Node\node-host\nodes\
	- There is a sample Bootstrapper Node called 'Gallery01 Bootstrapper' here for you to edit, with files already inside.
	- Rename the Bootstrapper Node folder by replacing Gallery01 with your Gallery Name.
	- Inside this folder, edit the 'nodeConfig.json' file to lookup your Gallery folder.
- Add your Gallery Nodes to c:\Nodel\node-host\galleries\
	- There is a sample Gallery Node here called Gallery01' for your to edit.
	- The Gallery Node name must match the name you just entered in the Bootstrapper nodeConfig.json file above.
	- Add your exhibit and device Nodes to the \Nodes folder inside the Gallery Node you just renamed.

If you want to create additional Galleries
- Copy and paste the existing Bootstrapper and Gallery folder into the same respective containing folders.
- Rename the Bootstrapper Node Name as above.
- Edit the nodeConfig.json file as appropriate.
- Edit the Gallery Node Name as appropriate.
- Copy/edit/delete/rename the device and exhibit Nodes inside the Gallery \Nodes folder as appropriate. 