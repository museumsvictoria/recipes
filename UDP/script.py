'''TCP Node'''

### Libraries required by this Node
import socket



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address","desc":"The IP address","schema":{"type":"string"}}')
param_port = Parameter('{"title":"Port","schema":{"type":"integer"}}')



### Functions used by this Node
def send_udp_string(msg):
  #open socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    sock.sendto(msg, (param_ipAddress, param_port))
  except socket.error, msg:
    print "error: %s\n" % msg
    local_event_Error.emit(msg)
  finally:
    if sock:
      sock.close()



### Local actions this Node provides
def local_action_Start(arg = None):
  """{"title":"Start","desc":"Start","group":"General"}"""
  print 'Action Start requested'
  send_udp_string('start\n')

def local_action_Stop(arg = None):
  """{"title":"Stop","desc":"Stop","group":"General"}"""
  print 'Action Stop requested'
  send_udp_string('stop\n')

def local_action_SetLogging(arg = None):
  """{"title":"SetLogging","desc":"SetLogging","schema":{"title":"Level","type":"string","enum":["file","normal"],"required":"true"},"group":"General"}"""
  print 'Action SetLogging requested - '+arg
  send_udp_string('logging:'+arg+'\n')

def local_action_SetVolume(arg = None):
  """{"title":"SetVolume","desc":"SetVolume","schema":{"title":"Level","type":"integer","required":"true"},"group":"General"}"""
  print 'Action SetVolume requested - '+str(arg)
  send_udp_string('volume:'+str(arg)+'\n')



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"An error has occured while communicating with the device.","group":"General"}')
# local_event_Error.emit(arg)



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'

