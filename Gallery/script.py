'''A gallery node that groups members for control propagation and status monitoring.'''

def main(arg=None):
  console.info('Started!')
  
# <!--- members and status support  

@after_main
def initMembersSupport():
  for memberInfo in lookup_parameter('members') or []:
    initMember(memberInfo)

MODES = ['Action & Signal', 'Signal Only']

param_members = Parameter({'title': 'Members', 'schema': {'type': 'array', 'items': {'type': 'object', 'properties': {
   'name': {'title': 'Node', 'type': 'string', 'order': 1},
   'hasStatus': {'type': 'boolean', 'title': 'Status', 'order': 3},
   'power': {'title': 'Power', 'type': 'object', 'order': 4, 'properties': {
     'mode': {'title': 'Mode', 'type': 'string', 'enum': MODES}}
   },
   'mute': {'title': 'Mute', 'type': 'object', 'order': 5, 'properties': {
     'mode': {'title': 'Mode', 'type': 'string', 'enum': MODES}
   }
}}}}})

def initMember(memberInfo):
  name = mustNotBeBlank('name', memberInfo['name'])

  if memberInfo.get('hasStatus'):
    initStatusSupport(name)

  if (memberInfo.get('power') or {}).get('mode') in MODES:
    initSignalSupport(name, memberInfo['power']['mode'], 'Power', ['On', 'Off'])
    
  if (memberInfo.get('mute') or {}).get('mode') in MODES:
    initSignalSupport(name, memberInfo['mute']['mode'], 'Mute', ['On', 'Off'])

membersBySignal = {}
    
def initSignalSupport(name, mode, signalName, states):
  members = getMembersInfoOrRegister(signalName, name)
  
  # establish local signals if haven't done so already
  localDesiredSignal = lookup_local_event('Desired %s' % signalName)
  
  if localDesiredSignal == None:
    localDesiredSignal, localResultantSignal = initSignal(signalName, mode, states)
  else:
    localResultantSignal = lookup_local_event(signalName)
    
  # establish a remote action
  if mode == 'Action & Signal':
    create_remote_action('%s %s' % (name, signalName), {'group': 'Members (%s)' % signalName, 'schema': {'type': 'string', 'enum': states}},
                         suggestedNode=name, suggestedAction=signalName)
  
  # establish a remote signal to receive status
  # signal status states include 'Partially ...' forms
  resultantStates = states + ['Partially %s' % s for s in states]
  
  localMemberSignal = Event('%s %s' % (name, signalName), {'title': '"%s" %s' % (name, signalName), 'group': 'Members', 'order': 9999+next_seq(), 'schema': {'type': 'string', 'enum': resultantStates}})
  
  def aggregateMemberSignals():
    shouldBeState = localDesiredSignal.getArg()
    partially = False
    
    for memberName in members:
      if lookup_local_event('%s %s' % (memberName, signalName)).getArg() != shouldBeState:
        partially = True
        
    localResultantSignal.emit('Partially %s' % shouldBeState if partially else shouldBeState)
    
  localMemberSignal.addEmitHandler(lambda arg: aggregateMemberSignals())
  localDesiredSignal.addEmitHandler(lambda arg: aggregateMemberSignals())
  
  def handleRemoteEvent(arg):
    if arg == True or arg == 1:
      arg = 'On'
    elif arg == False or arg == 0:
      arg = 'Off'
    
    localMemberSignal.emit(arg)
  
  create_remote_event('%s %s' % (name, signalName), handleRemoteEvent, {'group': 'Members (%s)' % signalName, 'order': next_seq(), 'schema': {'type': 'string', 'enum': resultantStates}},
                      suggestedNode=name, suggestedEvent=signalName)

def initSignal(signalName, mode, states):
  resultantStates = states + ['Partially %s' % s for s in states]
  
  localDesiredSignal = Event('Desired %s' % signalName, {'group': signalName, 'order': next_seq(), 'schema': {'type': 'string', 'enum': states}})
  
  localResultantSignal = Event('%s' % signalName, {'group': signalName, 'order': next_seq(), 'schema': {'type': 'string', 'enum': resultantStates}})
  
  def handler(complexArg):
    state = complexArg['state']
    noPropagate = complexArg.get('noPropagate')
    
    localDesiredSignal.emit(state)
    
    # for convenience, just emit the state as the status if no members are configured
    if isEmpty(lookup_parameter('members')):
      localResultantSignal.emit(state)
    
    else:
      if noPropagate:
        return
      
      for memberName in membersBySignal[signalName]:
        remoteAction = lookup_remote_action('%s %s' % (memberName, signalName))
        if remoteAction != None:
          remoteAction.call(complexArg)
          
  # create normal action (propagates)
  Action('%s' % signalName, lambda arg: handler({'state': arg}), {'group': signalName, 'order': next_seq(), 'schema': {'type': 'string', 'enum': states}})
  
  # create action with options (e.g. 'noPropagate')
  Action('%s Extended' % signalName, handler, {'group': 'Propagation', 'order': next_seq() + 999, 'schema': {'type': 'object', 'properties': {
           'state': {'type': 'string', 'enum': states, 'order': 3},
           'noPropagate': {'type': 'boolean', 'order': 2}}}})
  
  return localDesiredSignal, localResultantSignal

def getMembersInfoOrRegister(signalName, memberName):
  members = membersBySignal.get(signalName)
  if members == None:
    members = list()
    membersBySignal[signalName] = members
    
  members.append(memberName)

  return members

STATUS_SCHEMA = { 'type': 'object', 'properties': {
                    'level': { 'type': 'integer', 'order': 1 },
                    'message': {'type': 'string', 'order': 2 }
                } }

EMPTY_SET = {}
  
def initStatusSupport(name):
  # look up the members structure (assume
  members = getMembersInfoOrRegister('Status', name)
  
  # check if this node has a status yet
  selfStatusSignal = lookup_local_event('Status')
  if selfStatusSignal == None:
    selfStatusSignal = Event('Status', {'group': 'Status', 'order': next_seq(), 'schema': STATUS_SCHEMA})
    
  # status for the member
  memberStatusSignal = Event('%s Status' % name, {'title': '"%s" Status' % name, 'group': 'Members', 'order': 9999+next_seq(), 'schema': STATUS_SCHEMA})
  
  # suppression flag?
  memberStatusSuppressedSignal = Event('%s Status Suppressed' % name, {'title': '"%s" Suppression' % name, 'group': 'Members', 'order': 9999+next_seq(), 'schema': {'type': 'boolean'}})
  
  Action('Member %s Status Suppressed' % name, lambda arg: memberStatusSuppressedSignal.emit(arg), {'title': "%s"% name, 'group': 'Suppression', 'order': 9999+next_seq(), 'schema': {'type': 'boolean'}})
  
  def aggregateMemberStatus():
    aggregateLevel = 0
    aggregateMessage = 'OK'
    
    # for composing the aggegate message at the end
    msgs = []
    
    activeSuppression = False
    
    for memberName in members:
      suppressed = lookup_local_event('%s Status Suppressed' % memberName).getArg()
      
      memberStatus = lookup_local_event('%s Status' % memberName).getArg() or EMPTY_SET
      
      memberLevel = memberStatus.get('level')
      if memberLevel == None: # as opposed to the value '0'
        if suppressed:
          activeSuppression = True
          continue
          
        memberLevel = 99

      if memberLevel > aggregateLevel:
        # raise the level (if not suppressed)
        if suppressed:
          activeSuppression = True
          continue
        
        aggregateLevel = memberLevel
      
      memberMessage = memberStatus.get('message') or 'Has never been seen'
      if memberLevel > 0:
        if isBlank(memberMessage):
          msgs.append(memberName)
        else:
          msgs.append('%s: [%s]' % (memberName, memberMessage))
          
    if len(msgs) > 0:
      aggregateMessage = ', '.join(msgs)
      
    if activeSuppression:
      aggregateMessage = '%s (*)' % aggregateMessage
      
    selfStatusSignal.emit({'level': aggregateLevel, 'message': aggregateMessage})
      
  memberStatusSignal.addEmitHandler(lambda arg: aggregateMemberStatus())
  memberStatusSuppressedSignal.addEmitHandler(lambda arg: aggregateMemberStatus())
  
  def handleRemoteEvent(arg):
    memberStatusSignal.emit(arg)
  
  create_remote_event('%s Status' % name, handleRemoteEvent, {'group': 'Members (Status)', 'order': next_seq(), 'schema': STATUS_SCHEMA},
                       suggestedNode=name, suggestedEvent="Status")
  
# members and status support ---!>


# <!--- convenience functions

def mustNotBeBlank(name, s):
  if isBlank(s):
    raise Exception('%s cannot be blank')

  return s

def isBlank(s):
  if s == None or len(s) == 0 or len(s.strip()) == 0:
    return True
  
def isEmpty(o):
  if o == None or len(o) == 0:
    return True

# convenience functions ---!>

# <!--- customisation

param_legacy = Parameter({'title': 'Legacy Support', 'schema': {'type': 'boolean'}, 'order': next_seq() })

@after_main
def extendGroup():

  # Reintroduce the skeleton of the extended functions for muting in the scenario that these aren't selected as members.
  extendedMuteMeta = {'group': 'Propagation', 'order': next_seq() + 999, 'schema': {'type': 'object', 'properties': {
         'state': {'type': 'string', 'enum': ['On', 'Off'], 'order': 3},
         'noPropagate': {'type': 'boolean', 'order': 2}}}}

  def extendedMuteHandler(complexArg):
    state = complexArg['state']

    lookup_local_event('DesiredMute').emit(state)

    if state == 'On':
      lookup_local_event('MuteOn').emit()
    elif state == 'Off':
      lookup_local_event('MuteOff').emit()

  if param_legacy:
    # Contrust legacy events.
    Event('AllOn', {'group': 'Power'})
    Event('AllOff', {'group': 'Power'})
    Event('MuteOn', {'group': 'Mute'}) 
    Event('MuteOff', {'group': 'Mute'})

    # Construct legacy actions.
    Action('Enable', lambda arg: lookup_local_action('Power').call('On'), {'title': 'AllOn', 'group': 'Power'})
    Action('Disable', lambda arg: lookup_local_action('Power').call('Off'), {'title': 'AllOff', 'group': 'Power'})
    if lookup_local_action('Mute'):
      Action('MuteOn', lambda arg: lookup_local_action('Mute').call('On'), {'group': 'Mute'})
      Action('MuteOff', lambda arg: lookup_local_action('Mute').call('Off'), {'group': 'Mute'})
    else: # Reintroduce skeleton of mute
      Action('MuteOn', lambda arg: lookup_local_event('DesiredMute').emit('On'), {'group': 'Mute'})
      Action('MuteOff', lambda arg: lookup_local_event('DesiredMute').emit('Off'), {'group': 'Mute'})
      Action('MuteExtended', extendedMuteHandler, extendedMuteMeta)
      Event('DesiredMute', {'schema': {'type': 'string', 'enum': ['On', 'Off']}, 'order': 3, 'group':'Mute'})

    # Associate desires and events.
    def handlePower(arg):
      if arg == 'On':
        lookup_local_event('AllOn').emit()
        print 'PowerOn requested.'
      elif arg == 'Off':
        lookup_local_event('AllOff').emit()
        print 'PowerOff requested.'

    desirePower = lookup_local_event('DesiredPower')
    desirePower.addEmitHandler(handlePower)

    def handleMute(arg):
      if arg == 'On':
        lookup_local_event('MuteOn').emit()
        print 'MuteOn requested.'
      elif arg == 'Off':
        lookup_local_event('MuteOff').emit()
        print 'MuteOff requested'

    desireMute = lookup_local_event('DesiredMute')
    if desireMute:
      desireMute.addEmitHandler(handleMute)

# customisation ---!>

# <!--- text box

notes = list()

@after_main
def addTextModule():

  # Notepad
  Event('Notes', {'schema':{'type':'string','format':'long'},'group': 'Notepad'})

  # Take Note
  def handler(msg):
    notes.append(str(msg))

    str_of_notes = ""

    for note in notes:
      str_of_notes += ('* ' + note + '\n')

    lookup_local_event('Notes').emit(str_of_notes)

  meta = {'title': 'Take Note', 'schema':{'type':'string'},'group': 'Notepad'}

  Action('TakeNote', handler, meta)

  # Clear Note
  def handler(arg):
    lookup_local_event('Notes').emit('> Nothing to report...')
    global notes
    notes = []
  
  Action('ClearNotes', handler, {'title': 'Clear Notes', 'group': 'Notepad', 'order': '100'})

# text box ---!>