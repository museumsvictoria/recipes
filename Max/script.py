# Date:			2016.07.19
# Author:		Julien de-Sainte-Croix
# Version:		1.4
##  NEW: Sliders for channels.
###	Reciprocal UDP Node intended for 2-way communication with Max patches.

'''Max UDP Node'''

### Libraries required by this Node
import socket



### Parameters used by this Node
param_ipAddress = Parameter('{"title":"IP Address","desc":"The IP address.","schema":{"type":"string"}}')
param_port = Parameter('{"title":"Port","schema":{"type":"integer"}}')



### Functions used by this Node
transmit = Event('Transmit', {'schema': {'type': 'string'}, 'group': 'Communication'})
def send_udp_string(msg):
  #open socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    sock.sendto(msg, (param_ipAddress, param_port))
    transmit.emit(msg.strip('\n'))
  except socket.error, msg:
    print "error: %s\n" % msg
    local_event_Error.emit(msg)
  finally:
    if sock:
      sock.close()

def udp_ready():
    print 'UDP receiver started.'
    
def receive_udp_string(source, data):
	msg = str(data)
	print 'String received: ' + msg
	handle_message(msg)

# Create a new UDP object to receive input from target destination
udp_receiver = UDP(source=None, dest=None, ready=udp_ready, received=receive_udp_string)



### Events used by this Node
response = Event('Receive', {'schema': {'type': 'string'}, 'group': 'Communication'})
def handle_message(received):
  response.emit(received)



### Local actions this Node provides
def local_action_Custom(arg = None):
  """{"title":"Custom","desc":"Send custom string.","schema":{"type":"string"},"group":"General"}"""
  print 'String sent: ' +arg
  send_udp_string(arg)

def local_action_StartExample(arg = None):
  """{"title":"Start","desc":"Start example patch.","group":"Example Patch"}"""
  print 'String sent: StartExample'
  send_udp_string('StartExample')

def local_action_StopExample(arg = None):
  """{"title":"Stop","desc":"Stop example patch.","group":"Example Patch"}"""
  print 'String sent: StartExample'
  send_udp_string('StopExample')
  
def local_action_Preset1(arg = None):
  """{"title":"Preset1","desc":"Levels Preset 1","group":"Level Presets"}"""
  print 'String sent: Preset 1'
  send_udp_string('Preset 1')
  
def local_action_Preset2(arg = None):
  """{"title":"Preset2","desc":"Levels Preset 2","group":"Level Presets"}"""
  print 'String sent: Preset 2'
  send_udp_string('Preset 2')
  
def local_action_Preset3(arg = None):
  """{"title":"Preset3","desc":"Levels Preset 3","group":"Level Presets"}"""
  print 'String sent: Preset 1'
  send_udp_string('Preset 3')
  
def local_action_Preset4(arg = None):
  """{"title":"Preset4","desc":"Levels Preset 4","group":"Level Presets"}"""
  print 'String sent: Preset 4'
  send_udp_string('Preset 4')
  
  
def local_action_Channel01(arg = None):
  """{"title":"Channel01","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 120,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel01 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg)

def local_action_Channel02(arg = None):
  """{"title":"Channel02","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 158,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel02 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg)
  
def local_action_Channel03(arg = None):
  """{"title":"Channel03","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 158,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel03 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg)

def local_action_Channel04(arg = None):
  """{"title":"Channel04","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 158,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel04 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg)

def local_action_Channel05(arg = None):
  """{"title":"Channel05","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 158,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel05 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg)

def local_action_Channel06(arg = None):
  """{"title":"Channel06","desc":"Adjust level on Channel 01.","schema":{"title":"Drag slider to adjust level.","type":"integer","format":"range","min": 0, "max": 158,"required":"true"},"group":"Channels"}"""
  msg = ('Level ' + 'Channel06 ' + str(arg))
  print 'String sent: ' +msg
  send_udp_string(msg) 



### Local events this Node provides
local_event_Error = LocalEvent('{"title":"Error","desc":"An error has occured while communicating with the device.","group":"General"}')
# local_event_Error.emit(arg)



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'

  # bind receiver to localhost 
  udp_receiver.source = socket.gethostbyname(socket.gethostname()) + ':' + str(param_port)
