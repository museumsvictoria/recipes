'''PJNet Node'''

### Libraries required by this Node
import sys
import telnetlib



### Parameters used by this Node
param_ipAddress = ('{"title":"IP Address","schema":{"type":"string"}}')
PORT = 10000



### Functions used by this Node
def send_poweron(arg = None):
  tn = telnetlib.Telnet(param_ipAddress,PORT)

  tn.read_until("PASSWORD: ")
  tn.write("0000\r")
  tn.read_until("Hello")
  tn.write("C00\r")
  tn.write("exit\n")

def send_poweroff(arg = None):
  tn = telnetlib.Telnet(param_ipAddress,PORT)

  tn.read_until("PASSWORD: ")
  tn.write("0000\r")
  tn.read_until("Hello")
  tn.write("C01\r")
  tn.write("exit\n")



### Local actions this Node provides
def local_action_PowerOn(arg = None):
  """{"title":"PowerOn","desc":"PowerOn"}"""
  print 'Action PowerOn requested'
  send_poweron()

def local_action_PowerOff(arg = None):
  """{"title":"PowerOff","desc":"PowerOff"}"""
  print 'Action PowerOff requested'
  send_poweroff()



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'