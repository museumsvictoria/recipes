'''Event Tech Control Node'''

### Local events this Node provides
local_event_AllOn = LocalEvent('{"title":"AllOn","desc":"AllOn"}')
local_event_AllOff = LocalEvent('{"title":"AllOff","desc":"AllOff","Caution":"Are you sure?"}')
local_event_MuteOn = LocalEvent('{"title":"MuteOn","desc":"MuteOn"}')
local_event_MuteOff = LocalEvent('{"title":"MuteOff","desc":"MuteOff"}')

### Local actions this Node provides
def local_action_AllOn(arg = None):
  """{"title":"AllOn","desc":"AllOn"}"""
  local_event_AllOn.emit()

def local_action_AllOff(arg = None):
  """{"title":"AllOff","desc":"AllOff","Caution":"Are you sure?"}"""
  local_event_AllOff.emit()

def local_action_MuteOn(arg = None):
  """{"title":"MuteOn","desc":"MuteOn"}"""
  local_event_MuteOn.emit()

def local_action_MuteOff(arg = None):
  """{"title":"MuteOff","desc":"MuteOff"}"""
  local_event_MuteOff.emit()






### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'