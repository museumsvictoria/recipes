'''PJLink Queue Node'''

### Libraries required by this Node
from Queue import *
import threading
import atexit



### Local actions this Node provides
def local_action_PowerOn(x = None):
  '''{ "title": "PowerOn", "desc": "PowerOn", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOn', 'delay': 120})
  print 'Action PowerOn requested.'

def local_action_PowerOff(x = None):
  '''{ "title": "PowerOff", "desc": "PowerOff", "group": "Power" }'''
  queue.put({'function': 'remote_action_PowerOff', 'delay': 120})
  print 'Action PowerOff requested.'



### Remote actions this Node requires
remote_action_PowerOn = RemoteAction()
remote_action_PowerOff = RemoteAction()



### Classes required by this Node
class TimerClass(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.event = threading.Event()
  def run(self):
    while not self.event.isSet():
      if queue.empty() != True:
        job = queue.get()
        try:
          print "Calling command " + job['function']
          func = globals()[job['function']]
          arg = job['args'] if 'args' in job else ''
          func.call(arg)
          self.event.wait(job['delay'])
          queue.task_done()
        except Exception, e:
          print e
          print "Failed to call command " + job['function']
      else:
        self.event.wait(1)
  def stop(self):
      self.event.set()

queue = Queue()
th = TimerClass()

@atexit.register
def cleanup():
  print 'shutdown'
  th.stop()



### Main
def main():
  th.start()
  print 'Nodel script started.'