'''Gallery Node'''

### Libraries required by this Node
import socket
from time import sleep



### Parameters used by this Node
PORT = 9099
param_ipAddress = Parameter('{"title":"IP Address","schema":{"type":"string"}}')    # IP address of Show Control Lite process



### Functions used by this Node
def callOutcome(outcome):
  try:
    # initialise a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # bind to arbitrary socket
    sock.bind(('0.0.0.0', 0))
    # must not block forever waiting for response
    sock.settimeout(5)
    # local interface and port
    interface, returnPort = sock.getsockname()
    # get the local address (NOTE, this could be wrong on multihomed hosts)
    returnAddress = socket.gethostbyname(socket.gethostname())
    message = 'executeOutcome %s %s %s' % (outcome, returnAddress, returnPort)
    # send the message
    sock.sendto(message, (param_ipAddress, PORT))
    # receive a response
    data, addr = sock.recvfrom(512)
    print 'Received acknowledgement:', data
    if 'OK' in data:
      local_event_Success.emit()
    elif 'NOT_FOUND' in data:
      local_event_Error.emit('Outcome not found')
    else:
      local_event_Error.emit(('Unexpected response', data))
  except Exception, e:
    # probably a timeout but pass on exception regardless
    local_event_Error.emit(e)
  finally:
    sock.close()



### Local events this Node provides
local_event_Error = LocalEvent('{ "title":"Error","desc":"Request failed"}')
local_event_Success = LocalEvent('{ "title":"Success","desc":"Request was successful"}')



### Remote events this Node requires
def remote_event_TechAllOn(arg = None):
  """{"title":"TechAllOn","desc":"TechAllOn","group":"General"}"""
  print 'Remote event TechAllOn arrived.'
  callOutcome('AllOn')
  callOutcome('UnMute')

def remote_event_TechAllOff(arg = None):
  """{"title":"TechAllOff","desc":"TechAllOff","group":"General"}"""
  print 'Remote event TechAllOff arrived.'
  callOutcome('AllOff')

def remote_event_TechMuteOn(arg = None):
  """{"title":"TechMuteOn","desc":"TechMuteOn","group":"General"}"""
  print 'Remote event TechMuteOn arrived.'
  callOutcome('Mute')

def remote_event_TechMuteOff(arg = None):
  """{"title":"TechMuteOff","desc":"TechMuteOff","group":"General"}"""
  print 'Remote event TechMuteOff arrived.'
  callOutcome('UnMute')



### Main
def main(arg = None):
  # Start your script here.
  print 'Nodel script started.'